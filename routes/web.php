<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/create/declaration','DeclarationController@create')->name('create/declaration');

Route::get('/select/declaration','DeclarationController@index')->name('select-declaration');
Route::post('/save/declaration','DeclarationController@store');

Route::resource('declaracion-isr', 'DeclarationISRController');

Route::get('/declaracion-ISR','DeclarationISRController@index')->name('declaration/isr');
Route::get('/declaracion-IVA','DeclarationController@index')->name('declaration/iva');

