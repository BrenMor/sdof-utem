$('#target2').hide(); //muestro mediante clase
$(document).ready(function () {
    $("#mostrar").on("click", function () {
        $('#target1').show(); //muestro mediante id
        $('#target2').hide(); //muestro mediante clase
    });
    $("#ocultar").on("click", function () {
        $('#target1').hide(); //oculto mediante id
        $('#target2').show(); //muestro mediante clase
    });
});

$(document).ready(function(){
    $('#show_me').hide(); //muestro mediante clase
    $('#mostrar_segun_html').change(function(){
        var sel = $( "#mostrar_segun_html option:selected" ).html();
        if (sel == "Si") {
            $('#show_me').show();
        } else {
            $('#show_me').hide();
        }
    })

})

$(document).ready(function(){
    $('#mostrar_desgloce').hide(); //muestro mediante clase
    $('#estimulos_fiscales').change(function(){
        var sel = $( "#estimulos_fiscales option:selected" ).html();
        if (sel == "Si") {
            $('#mostrar_desgloce').show();
        } else {
            $('#mostrar_desgloce').hide();
        }
    })

})


$(document).ready(function(){
    $('#parcialidades').hide(); //muestro mediante clase
    $('#opta_parcialidades').change(function(){
        var sel = $( "#opta_parcialidades option:selected" ).html();
        if (sel == "Si") {
            $('#parcialidades').show();
        } else {
            $('#parcialidades').hide();
        }
    })

})
