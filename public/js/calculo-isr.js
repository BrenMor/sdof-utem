function calculo()
{
    var getAccountsTransactionsStatement = "SELECT transactionId, fromAccount, toAccount, transactionDate, transactionAmount, transactionType " +
        "FROM accounttransactions " +
        "WHERE accounttransactions.fromAccount = ? AND accounttransactions.toAccount = ? " +
        "ORDER BY transactionDate DESC " +
        "LIMIT 20;";

    document.getElementById("totalIngresos").value = document.getElementById("ingresosPeriodo").value;

    document.getElementById("total_compras_gastos").value = document.getElementById("compras_gastos").value;

    var num1 = document.getElementById("totalIngresos").value;
    var num2 = document.getElementById("total_compras_gastos").value;

    document.getElementById("baseGravable").value = num1 - num2;

    document.getElementById("pagos-definidos").value =  num1 - num2;


let declaration = document.querySelector('#pagos-definidos').dataset.declaration;
declaration = JSON.parse(declaration);
console.log(declaration);

}

$.getJSON('name', function(data){
    $('#pagos').vectorMap({
        markers: data,
    })
});

var getAccountsTransactionsStatement = "SELECT transactionId, fromAccount, toAccount, transactionDate, transactionAmount, transactionType " +
    "FROM accounttransactions " +
    "WHERE accounttransactions.fromAccount = ? AND accounttransactions.toAccount = ? " +
    "ORDER BY transactionDate DESC " +
    "LIMIT 20;";

//Invoke prepared SQL query and return invocation result
function getAccountTransactions1(fromAccount, toAccount){
    return MFP.Server.invokeSQLStatement({
        preparedStatement : getAccountsTransactionsStatement,
        parameters : [fromAccount, toAccount]
    });
}
