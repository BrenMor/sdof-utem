<?php

namespace App\Http\Controllers;

use App\Declarationisr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DeclarationISRController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tarifas = DB::table('tarifas_pagos_provisionales')->get();

        $declaration = DB::table('declaration_detail')
        ->join('months', 'months.id', '=', 'declaration_detail.periodId')
        ->select('name')
        ->get()->last();


        return view('declarations-forms.content-isr', compact('tarifas', 'declaration'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Declarationisr  $declarationisr
     * @return \Illuminate\Http\Response
     */
    public function show(Declarationisr $declarationisr)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Declarationisr  $declarationisr
     * @return \Illuminate\Http\Response
     */
    public function edit(Declarationisr $declarationisr)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Declarationisr  $declarationisr
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Declarationisr $declarationisr)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Declarationisr  $declarationisr
     * @return \Illuminate\Http\Response
     */
    public function destroy(Declarationisr $declarationisr)
    {
        //
    }
}
