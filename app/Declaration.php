<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Declaration extends Model
{
    protected $table = "declaration_detail";
    protected $fillable = ['userId', 'declarationType', 'periodicity', 'periodId', 'excercise'];
}
