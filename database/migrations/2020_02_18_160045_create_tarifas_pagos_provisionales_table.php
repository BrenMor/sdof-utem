<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTarifasPagosProvisionalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tarifas_pagos_provisionales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('month');
            $table->integer('lowerLimit');
            $table->integer('upperLimit');
            $table->integer('fixedFee');
            $table->integer('excess');
            $table->integer('periodId');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tarifas_pagos_provisionales');
    }
}
