@extends('layouts.app')

@section('content')
    <link href="{{ asset('css/welcome.css') }}" rel="stylesheet">
    <div class="container">
        <div class="card-body">
            <p class="card-title">Tus</p>
            <p class="card-subtitle">practicas</p>
            <p class="card-text">más agil y sencillo</p>
            <p class="card-image"></p>
        </div>
    </div>
@endsection
