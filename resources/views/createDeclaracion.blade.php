@extends('layouts.app')

@section('content')

    <link rel="stylesheet" href="/css/div-oculto.css">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Declaración Provisional</div>
                    <div class="card-body">
                        @if(Auth::user())
                            <div class="container">
                                <h3>*Declaración Provisional o Definitiva de Impuestos Federales</h3>
                                <form method="post" action="{{url('/save/declaration')}}" name="formulario1">
                                    <input type="hidden" value="{{ csrf_token() }}" name="_token">
                                    <input type="hidden" value="{{ Auth::user()->id }}" name="userId">
                                    <div class="row">
                                        <div class="col-sm-6 form-group">
                                            <label class="col-sm-4 control-label">*RFC</label>
                                            <input type="text" class="form-control"
                                                   placeholder="{{ Auth::user()->rfc }}"readonly>


                                        </div>
                                        <div class="col-sm-6 form-group">
                                            <label class="col-sm-4 control-label">*Tipo de Declaracion</label>
                                            <select type="text" class="form-control" name="declarationType">
                                                <option value="Normal">Normal</option>
                                                <option value="Complementaria">Complementaria</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6 form-group">
                                            <label for="date" class="col-sm-4 control-label">*Periodicidad</label>
                                            <select type="text" class="form-control" name="periodicity">
                                                <option value="null">Seleccionar...</option>
                                                <option value="mensual">1.Mensual</option>
                                                <option value="trimestral">2.Trimestral</option>
                                                <option value="cuatrimestral">3.Cuatrimestral</option>
                                                <option value="semestral(a)">4.Semestral (A)</option>
                                                <option value="semestral(b)">5.Semestral (B) Liquidación</option>
                                                <option value="ajuste">6.Ajuste</option>
                                                <option value="del ejercicio">7.Del ejercicio</option>
                                                <option value="sin periodo">8.Sin periodo</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6 form-group">
                                            <label for="declaration" class="col-sm-4 control-label">*Periodo</label>
                                            <select type="text" class="form-control" name="periodId">
                                                <option value="null">Seleccionar...</option>
                                                @foreach($months as $month)
                                                <option value="{{$month->id}}">{{$month->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-6 form-group">
                                            <label for="declaration" class="col-sm-4 control-label">*Ejercicio</label>
                                            <select type="text" class="form-control" name="excercise">
                                                <option value="2019">2019</option>
                                                <option value="2020">2020</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <sub>*Campos Oblgatorios</sub>
                                    </div>
                                    <br>

                                    <div id="contenido">
                                        <h3>Obligaciones Registradas</h3>

                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th scope="col"></th>
                                                <th scope="col">Descripcion</th>
                                                <th scope="col">Fecha de Vencimiento</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td scope="row"><input type="checkbox" class="form-check-input"
                                                                       id="exampleCheck1" checked disabled></td>
                                                <td>ISR PERSONAS FÍSICAS. ACTIVIDAD EMPRESARIAL Y PROFESIONAL</td>
                                                <td>22/07/2018</td>
                                            </tr>
                                            <tr>
                                                <th scope="row"><input type="checkbox" class="form-check-input"
                                                                       id="exampleCheck1" checked disabled></th>
                                                <td>IMPUESTO AL VALOR AGREGADO</td>
                                                <td>22/07/2018</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!--<button type="button" class="btn btn-success" id="hide" >Atrás</button>-->
                                    </div>
                                    <div class="titulo_boton" align="right">
                                        <button type="button" class="btn btn-success" onclick="mostrarOcultar();">
                                            Siguiente
                                        </button>
                                    </div>


                                    <!-- ----------- Modal mensaje de advertencia ------------->
                                    <div class="modal" id="modalMensaje" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content  mensaje">
                                                <div class="modal-body justificado">
                                                    <p>Para continuar brindando facilidades a los contribuyentes
                                                        personas físicas con
                                                        ingresos anuales hasta 4 millones, se les exime de la
                                                        presentación de la
                                                        declaración informativa de operaciones con terceros (DIOT) y del
                                                        envío de la
                                                        Contabilidad Electrónica</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit"  class="btn btn-success">Continuar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <script !src="">

        //oculto por defecto
        element = document.getElementById('contenido');
        element.style.display = 'none';

        function mostrarOcultar() {
            element = document.getElementById('contenido');
            estado = element.style.display;
            if (estado == 'none') {
                element.style.display = 'block'
            } else {
                $('#modalMensaje').modal('show');
            }

        }//end function mostrarOcultar
    </script>

@endsection
