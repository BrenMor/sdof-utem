<div class="modal fade" id="form-IVA">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 id="title-form" class="modal-title">DECLARACIÓN PROVISIONAL O DEFINITIVA DE IMPUESTOS FEDERALES</h4>
            </div>
            <div class="modal-body">
                <h5 id="subtittle-form">DETERMINACIÓN DEL IMPUESTO AL VALOR AGREGADO</h5>
                <form class="form-horizontal">
                    <div class="form-group form-group-sm">
                        <!-- left column -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-6 control-label">INGRESOS POR LOS QUE COBRÓ IVA A LA TASA DEL 16%</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="ingresos-iva" placeholder="registrar ingresos">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">INGRESOS POR LOS QUE COBRÓ IVA A LA TASA DEL 0%</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_subname" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">INGRESOS POR LOS QUE NO COBRÓ IVA (EXENTOS)</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_name" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">IVA COBRADO DEL PERIODO</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_name" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">IVA QUE LE RETUVIERON</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_name" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">IVA ACREDITABLE POR IMPORTACIÓN DE BIENES TANGIBLES</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_name" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">IVA ACREDITABLE POR IMPORTACIÓN DE BIENES INTANGIBLES</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_name" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">IVA ACREDITABLE POR COMPRAS Y GASTOS</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_name" placeholder="">
                                </div>
                            </div>
                        </div>

                        <!-- right column -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-6 control-label">OTRAS CANTIDADES A CARGO DEL CONTRIBUYENTE</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_name" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="new_subname" class="col-sm-6 control-label">OTRAS CANTIDADES A FAVOR DEL CONTRIBUYENTE</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_subname" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">CANTIDAD A CARGO</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_name" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">SALDO A FAVOR</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_name" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">DEVOLUCIÓN INMEDIATA OBTENIDA</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_name" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">SALDO A FAVOR DEL IVA DEL PERIODO</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_name" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">SALDO A FAVOR DEL IVA DE PERIODOS ANTERIORES (SIN EXCEDER DE LA CANTIDAD A CARGO)</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_name" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">IMPUESTO A CARGO</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_name" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- *********************************************************************** -->
                    <h5 id="subtittle-form">DATOS PARA LA DEVOLUCIÓN DEL SALDO A FAVOR DEL IVA</h5>
                    <div class="form-group form-group-sm">
                        <!-- left column -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-6 control-label">OPTA POR</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_name" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">NOMBRE DEL BANCO</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_subname" placeholder="">
                                </div>
                            </div>
                        </div>

                        <!-- right column -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-6 control-label">SELECCIONE SU NÚMERO DE CUENTA "CLABE"</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_name" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="new_subname" class="col-sm-6 control-label">NÚMERO DE CUENTA "CLABE"</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_subname" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- *********************************************************************** -->
                    <h5 id="subtittle-form">DETALLE DEL PAGO R21 <br>IMPUESTO AL VALOR AGREGADO</h5>
                    <div class="form-group form-group-sm">
                        <!-- left column -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-6 control-label">A CARGO</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_name" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">A FAVOR</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_subname" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">PARTE ACTUALIZADA</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_subname" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">RECARGOS</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_subname" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">MULTA POR CORRECCIOŃ</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_subname" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">TOTAL DE CONTRIBUCIONES</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_subname" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">¿DESEA DISMINUIR SU TOTAL DE CONTRIBUCIONES CON ALGÚN CONCEPTO</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_subname" placeholder="">
                                </div>
                            </div>
                        </div>

                        <!-- right column -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-6 control-label">FECHA DEL PAGO UTILIZADO CON ANTERIORIDAD</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_name" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="new_subname" class="col-sm-6 control-label">MONTO PAGADO CON ANTERIORIDAD</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_subname" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">IMPORTE PAGADO EN LAS ÚLTIMAS  HORAS</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_subname" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">¿OPTA POR PAGAR PARCIALIDADES?</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_subname" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">CANTIDAD A FAVOR</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_subname" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">CANTIDAD A PAGAR</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="new_subname" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wrapper">
                        <button id="register-IVA" type="button" class="btn btn-primary">Enviar declaración al simulador</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>