@extends('layouts.app')

@section('content')
    <link href="{{ asset('css/select-declarations.css') }}" rel="stylesheet">

    @if(Auth::user())
        <div class="container">
            <div class="row" id="contentido">
                <div class="col content-der">
                    <p id="title-otras"><strong>Otras obligaciones</strong></p>
                    <div class="row selecIndividual">
                        <a class="btn-largo" href="{{ route('declaration/isr') }}">
                            <table class="col" id="tr-title">
                                <tr>
                                    <td><strong>ISR PERSONAS FÍSICAS: ACTIVIDAD EMPRESARIAL</strong></td>
                                </tr>
                                <tr class="tr-content">
                                    <td>Cantidad a pagar en efectivo: <input id="valor2" class="col-2" value="$0" disabled></td>
                                </tr>
                            </table>
                        </a>
                    </div>

                    <div class="row selecIndividual">
                        <a class="btn-largo" href="{{ route('declaration/iva') }}">
                            <table class="col" id="tr-title">
                                <tr>
                                    <td><strong>IMPUESTO AL VALOR AGREGADO</strong></td>
                                </tr>
                                <tr class="tr-content">
                                    <td>Cantidad a pagar en efectivo: <input id="valor2" class="col-2" value="$0" disabled></td>
                                </tr>
                            </table>
                        </a>
                    </div>

                    <div class="card pago">
                        <div class="card-header" id="total">
                            Total a pagar:
                        </div>
                        <div class="card-body texto-right">
                            <strong><input id="valor" class="card-text" value="$0" disabled></strong>
                        </div>
                    </div>
                </div>

                <div class="col selectForm">
                    <p id="title-instructions"><strong>Descripción de los pasos para el llenado de la
                            declaración</strong></p>
                    <ol id="contenido-instructions">
                        <li value="1">Ingresa a cada una de las obligaciones y captura la información que se solicita
                            correspondiente al periodo que está declarando.
                        </li>
                        <li>Seleccionar el botón "Enviar Declaración" para realizar el envío.</li>
                        <li>Posteriormente el sistema generará el acuse de recibo de la declaración realizada.</li>
                    </ol>
                </div>
            </div>
        </div>
    @endif
@endsection
