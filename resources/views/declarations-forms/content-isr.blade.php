@extends('layouts.app')

@section('content')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <link href="{{ asset('css/forms-declarations.css') }}" rel="stylesheet">

    <div class="container">

        <div class="row">
            <div class="col-6">
                <p id="title"><strong>ISR PERSONAS FÍSICAS: ACTIVIDAD EMPRESARIAL</strong></p>
            </div>
        </div>

        <div class="row">
            <div class="col-4" id="buttons">
                <a type="button" href="{{route('declaracion-isr.store')}}" class="btn">Guardar</a>
                <a type="button" href="{{route('select-declaration')}}" class="btn">Instrucciones</a>
                <a type="button" href="{{route('select-declaration')}}" class="btn">Menú principal</a>
            </div>
        </div>


        <div class="row justify-content-md-center" id="titulos-isr">
            <div class="col-2">
                <input type="button" id="mostrar" name="boton1" value="Determinación del Impuesto">
            </div>
            <div class="col">
                <input type="button" id="ocultar" name="boton2" value="Determinación de Pago">
            </div>
        </div>

        <div class="row isr" id="contenido-isr">
            <div class="col-8">
                <form>
                    <div id="target1">

                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-6 col-form-label">Ingresos del periodo</label>
                            <div class="col-sm-5">
                                <input class="form-control form-control-sm" type="text" name="ingresosPeriodo" id="ingresosPeriodo" onkeyup="calculo();">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-6 col-form-label">Total de ingresos</label>
                            <div class="col-sm-5">
                                <input class="form-control form-control-sm" type="text" placeholder="0" disabled name="totalIngresos" id="totalIngresos">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-6 col-form-label">Compras y gastos del
                                periodo</label>
                            <div class="col-sm-5">
                                <input class="form-control form-control-sm" type="text" name="compras_gastos" id="compras_gastos" onkeyup="calculo();">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-6 col-form-label">Total de compras y gastos</label>
                            <div class="col-sm-5">
                                <input class="form-control form-control-sm" type="text" placeholder="0" disabled name="total_compras_gastos" id="total_compras_gastos">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-6 col-form-label">¿Tienes estimulos
                                fiscales?</label>
                            <div class="col-sm-5">
                                <select class="form-control form-control-sm" id="estimulos_fiscales">
                                    <option id="option" selected="true" disabled="disabled">Sin selección</option>
                                    <option>Si</option>
                                    <option>No</option>
                                </select>
                            </div>
                        </div>

                        <!---------------------  Aparecer estas preguntas si la contestacion es si  ---------------------------->

                        <div id="mostrar_desgloce">
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-6 col-form-label">Donación de bienes básicos
                                    para
                                    la subsistencia humana</label>
                                <div class="col-sm-5">
                                    <input class="form-control form-control-sm" type="text" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-6 col-form-label">Empleo de personas con
                                    discapacidad y / o adultos mayores</label>
                                <div class="col-sm-5">
                                    <input class="form-control form-control-sm" type="text" placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-6 col-form-label">Estimulo por deducción
                                    inmediata</label>
                                <div class="col-sm-5">
                                    <input class="form-control form-control-sm" type="text" placeholder="">
                                </div>
                            </div>
                        </div>

                        <!----------------------------------------------------------------------------------------------------->

                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-6 col-form-label">Deducción de inversiones de
                                ejercicios anteriores</label>
                            <div class="col-sm-5">
                                <input class="form-control form-control-sm" type="text" placeholder="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-6 col-form-label">PTU pagada ene el
                                ejercicio</label>
                            <div class="col-sm-5">
                                <input class="form-control form-control-sm" type="text" placeholder="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-6 col-form-label">Pérdidas fiscales de ejercicios
                                anteriores</label>
                            <div class="col-sm-5">
                                <input class="form-control form-control-sm" type="text" placeholder="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-6 col-form-label">Base gravable del pago
                                provisional</label>
                            <div class="col-sm-5">
                                <input class="form-control form-control-sm" type="text" placeholder="0" disabled name="baseGravable" id="baseGravable">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-6 col-form-label">ISR causado</label>
                            <div class="col-sm-5">
                                <input class="form-control form-control-sm" value="{{$declaration->name}}" id="period" type="text" placeholder="0" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-6 col-form-label">Pagos provisionales efectuados
                                con anterioridad</label>
                            <div class="col-sm-5">
                                <input class="form-control form-control-sm" type="text" id="pagos-definidos" data-user=@json($declaration)>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-6 col-form-label">impuesto retenido</label>
                            <div class="col-sm-5">
                                <input class="form-control form-control-sm" type="text" placeholder="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-6 col-form-label">ISR a cargo</label>
                            <div class="col-sm-5">
                                <input class="form-control form-control-sm" type="text" placeholder="0" disabled>
                            </div>
                        </div>

                    </div>

                    <!------------------------------ Formulario metodo de pago ------------------------------>

                    <div id="target2">
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-6 col-form-label">A cargo</label>
                            <div class="col-sm-5">
                                <input class="form-control form-control-sm" type="text" placeholder="0" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-6 col-form-label">Total de contribuciones</label>
                            <div class="col-sm-5">
                                <input class="form-control form-control-sm" type="text" placeholder="0" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-6 col-form-label">¿Desea disminuir su total de
                                contribuciones con algún concepto?</label>
                            <div class="col-sm-5">
                                <select class="form-control form-control-sm" id="mostrar_segun_html">
                                    <option value="no">Si</option>
                                    <option value="yes" selected="true">No</option>
                                </select>
                            </div>
                        </div>

                        <!---------------------- Desgloce opción si --------------------------------------->
                        <div id="show_me">
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-6 col-form-label">Subsidio para el
                                    empleo</label>
                                <div class="col-sm-5">
                                    <input class="form-control form-control-sm" type="text">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-6 col-form-label">Impuesto a los depósitos en
                                    efectivo acreditable</label>
                                <div class="col-sm-5">
                                    <input class="form-control form-control-sm" type="text"
                                           placeholder="0">
                                </div>
                            </div>

                            <div class="row" id="detalle-content">
                                <div class="col col-lg-6">
                                    <label for="inputPassword">Compensaciones</label>
                                </div>
                                <div class="col col-lg-5">
                                    <input class="form-control form-control-sm" type="text"
                                           placeholder="" disabled>
                                </div>
                                <div class="col col-1" id="detalle-button">
                                    <button type="button" class="btn btn-success">Detalle</button>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-6 col-form-label">Crédito IEPS diésel, sector
                                    primario y número</label>
                                <div class="col-sm-5">
                                    <input class="form-control form-control-sm" type="text"
                                           placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-6 col-form-label">Diésel automotríz para
                                    transporte</label>
                                <div class="col-sm-5">
                                    <input class="form-control form-control-sm" type="text"
                                           placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-6 col-form-label">Uso de infraestructura,
                                    carretera de cuota</label>
                                <div class="col-sm-5">
                                    <input class="form-control form-control-sm" type="text"
                                           placeholder="">
                                </div>
                            </div>

                            <div class="row" id="detalle-content">
                                <div class="col col-lg-6">
                                    <label for="inputPassword">Otros estímulos</label>
                                </div>
                                <div class="col col-lg-5">
                                    <input class="form-control form-control-sm" type="text"
                                           placeholder="" disabled>
                                </div>
                                <div class="col col-1" id="detalle-button">
                                    <button type="button" class="btn btn-success">Detalle</button>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-6 col-form-label">Diésel marino</label>
                                <div class="col-sm-5">
                                    <input class="form-control form-control-sm" type="text"
                                           placeholder="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-6 col-form-label">Total de aplicaciones</label>
                                <div class="col-sm-5">
                                    <input class="form-control form-control-sm" type="text"
                                           placeholder="0" disabled>
                                </div>
                            </div>
                        </div>

                        <!-----------------------  final de desgloce ------------------------------------------------>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-6 col-form-label">Cantidad a cargo</label>
                            <div class="col-sm-5">
                                <input class="form-control form-control-sm" type="text" placeholder="0" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-6 col-form-label">¿Opta por pagar en
                                parcialidades?</label>
                            <div class="col-sm-5">
                                <select class="form-control form-control-sm" id="opta_parcialidades" disabled>
                                    <option value="no">Si</option>
                                    <option value="yes" selected="true">No</option>
                                </select>
                            </div>
                        </div>

                        <!----------------------------  Desgloce de opcion si parcialidades ---------------------->

                        <div id="parcialidades">
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-6 col-form-label">Cantidad a cargo</label>
                                <div class="col-sm-5">
                                    <input class="form-control form-control-sm" type="text" placeholder="0" disabled>
                                </div>
                            </div>

                        </div>

                        <!--------------------------- Fin de desgloce parcialidades -------------------------------->


                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-6 col-form-label">Cantidad a pagar</label>
                            <div class="col-sm-5">
                                <input class="form-control form-control-sm" type="text" placeholder="" disabled>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/contenido-isr.js') }}" defer></script>
    <script src="{{ asset('js/calculo-isr.js') }}" defer></script>
@endsection


